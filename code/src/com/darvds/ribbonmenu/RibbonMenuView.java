package com.darvds.ribbonmenu;


import java.util.ArrayList;

import org.xmlpull.v1.XmlPullParser;

import android.content.Context;
import android.content.res.XmlResourceParser;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Switch;
import android.widget.TextView;
import com.toplion.wisehome.R;

//Modifying a library directly to fit our need is a very bad practice.
//This whole class should be removed (at some point) and changed with standard Navigation Drawers
public class RibbonMenuView extends LinearLayout {

	private String PREF_NAME = "watchZPrefs";
	private String CALL_NOTIFY_NAME = "watchZCallNotify";
	private String SMS_NOTIFY_NAME = "watchZSmsNotify";
	private String EMAIL_NOTIFY_NAME = "watchZEmailNotify";
	private ListView rbmListView;
	private View rbmOutsideView;
	
	private iRibbonMenuCallback callback;
	
	private static ArrayList<RibbonMenuItem> menuItems;
	private Context mContext;
	private boolean bWithSwitchButton = true;
	
	public RibbonMenuView(Context context) {
		super(context);
		mContext = context;
		
		load();
	}
	
	public RibbonMenuView(Context context, AttributeSet attrs) {
		super(context, attrs);
		mContext = context;
		load();
	}


	public void setWithSwitchButton(boolean WithSwitchButton)
	{
		bWithSwitchButton = WithSwitchButton;
	}
	public boolean getWithSwitchButton()
	{
		return bWithSwitchButton;
	}
	
	private void load(){
		
		if(isInEditMode()) return;
			
		inflateLayout();		
		
		initUi();
		
		
	}
	
	
	private void inflateLayout(){
		
		
		
		
		try{
			LayoutInflater.from(getContext()).inflate(R.layout.rbm_menu, this, true);
			} catch(Exception e){
				
			}	
		
		
	}
	
	private void initUi(){
		
		rbmListView = (ListView) findViewById(R.id.rbm_listview);
		rbmOutsideView = (View) findViewById(R.id.rbm_outside_view);
				
		rbmOutsideView.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				hideMenu();
				
			}
		});
		
		
		rbmListView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				
				if(callback != null)					
					callback.RibbonMenuItemClick(menuItems.get(position).id);
				
				hideMenu();
			}
			
		});
			
		
	}
	
	
	public void setMenuClickCallback(iRibbonMenuCallback callback){
		this.callback = callback;
	}
	
	public void setMenuItems(int menu){
		
		parseXml(menu);
		
		if(menuItems != null && menuItems.size() > 0)
		{
			rbmListView.setAdapter(new Adapter());
			
		}
		
		
		
	
	}
	
	
	public void setBackgroundResource(int resource){
		rbmListView.setBackgroundResource(resource);
		
	}
	
	
	
	
	public void showMenu(){
		rbmOutsideView.setVisibility(View.VISIBLE);	
				
		rbmListView.setVisibility(View.VISIBLE);	
		rbmListView.startAnimation(AnimationUtils.loadAnimation(getContext(), R.anim.rbm_in_from_left));
		
	}
	
	
	public void hideMenu(){
		
		rbmOutsideView.setVisibility(View.GONE);
		rbmListView.setVisibility(View.GONE);	
		
		rbmListView.startAnimation(AnimationUtils.loadAnimation(getContext(), R.anim.rbm_out_to_left));
		
	}
	
	
	public void toggleMenu(){
		
		if(rbmOutsideView.getVisibility() == View.GONE){
			showMenu();
		} else {
			hideMenu();
		}
	}
	
	
	private void parseXml(int menu){
		
		menuItems = new ArrayList<RibbonMenuView.RibbonMenuItem>();
		
		
		try{
			XmlResourceParser xpp = getResources().getXml(menu);
			
			xpp.next();
			int eventType = xpp.getEventType();
			
			
			while(eventType != XmlPullParser.END_DOCUMENT){
				
				if(eventType == XmlPullParser.START_TAG){
					
					String elemName = xpp.getName();
						
					
					
					if(elemName.equals("item")){
											
						
						String textId = xpp.getAttributeValue("http://schemas.android.com/apk/res/android", "title");
						String iconId = xpp.getAttributeValue("http://schemas.android.com/apk/res/android", "icon");
						String resId = xpp.getAttributeValue("http://schemas.android.com/apk/res/android", "id");
						
						
						RibbonMenuItem item = new RibbonMenuItem();
						item.id = Integer.valueOf(resId.replace("@", ""));
						item.text = resourceIdToString(textId);
						item.icon = Integer.valueOf(iconId.replace("@", ""));
						
						menuItems.add(item);
						
					}
					
					
					
				}
				
				eventType = xpp.next();
				
				
			}
			
			
		} catch(Exception e){
			e.printStackTrace();
		}
		
	}
	
	
	
	
	
	private String resourceIdToString(String text){
		
		if(!text.contains("@")){
			return text;
		} else {
									
			String id = text.replace("@", "");
									
			return getResources().getString(Integer.valueOf(id));
			
		}
		
	}
	
	
	public boolean isMenuVisible(){		
		return rbmOutsideView.getVisibility() == View.VISIBLE;		
	}
	
		
	
	
	@Override 
	protected void onRestoreInstanceState(Parcelable state)	{
	    SavedState ss = (SavedState)state;
	    super.onRestoreInstanceState(ss.getSuperState());

	    if (ss.bShowMenu)
	        showMenu();
	    else
	        hideMenu();
	}
	
	

	@Override 
	protected Parcelable onSaveInstanceState()	{
	    Parcelable superState = super.onSaveInstanceState();
	    SavedState ss = new SavedState(superState);

	    ss.bShowMenu = isMenuVisible();

	    return ss;
	}

	static class SavedState extends BaseSavedState {
	    boolean bShowMenu;

	    SavedState(Parcelable superState) {
	        super(superState);
	    }

	    private SavedState(Parcel in) {
	        super(in);
	        bShowMenu = (in.readInt() == 1);
	    }

	    @Override
	    public void writeToParcel(Parcel out, int flags) {
	        super.writeToParcel(out, flags);
	        out.writeInt(bShowMenu ? 1 : 0);
	    }

	    public static final Parcelable.Creator<SavedState> CREATOR
	            = new Parcelable.Creator<SavedState>() {
	        public SavedState createFromParcel(Parcel in) {
	            return new SavedState(in);
	        }

	        public SavedState[] newArray(int size) {
	            return new SavedState[size];
	        }
	    };
	}
	
	
	
	class RibbonMenuItem{
		
		int id;
		String text;
		int icon;
		
	}
	
	
	
	private class Adapter extends BaseAdapter {

		private LayoutInflater inflater;
		
		public Adapter(){
			inflater = LayoutInflater.from(getContext());
		}
		
		
		
		@Override
		public int getCount() {
			
			return menuItems.size();
		}

		@Override
		public Object getItem(int position) {
			
			return null;
		}

		@Override
		public long getItemId(int position) {
			
			return 0;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			//Modifying a library directly to fit our need is a very bad practice.
			//This whole class should be removed (at some point) and changed with standard Navigation Drawers
		
			final ViewHolder holder;
			final int itemindex = position;
			
			if(bWithSwitchButton)
			{
			if(convertView == null || convertView instanceof TextView){
				convertView = inflater.inflate(R.layout.rbm_item, null);
				
				holder = new ViewHolder();
				holder.image = (ImageView) convertView.findViewById(R.id.rbm_item_icon);
				holder.text = (TextView) convertView.findViewById(R.id.rbm_item_text);
				holder.switchView = (Switch)	convertView.findViewById(R.id.callSwitch);
				
				convertView.setTag(holder);
			
			} else {
			
				holder = (ViewHolder) convertView.getTag();
			}
			
			holder.image.setImageResource(menuItems.get(position).icon);
			holder.text.setText(menuItems.get(position).text);
			//holder.switchView
			if(itemindex ==0)
			{
				holder.switchView.setChecked(mContext.getSharedPreferences(PREF_NAME, android.content.Context.MODE_PRIVATE).getBoolean(CALL_NOTIFY_NAME, false));
			}
			if(itemindex ==1)
			{
				holder.switchView.setChecked(mContext.getSharedPreferences(PREF_NAME, android.content.Context.MODE_PRIVATE).getBoolean(SMS_NOTIFY_NAME, false));
			}
			if(itemindex ==2)
			{
				holder.switchView.setChecked(mContext.getSharedPreferences(PREF_NAME, android.content.Context.MODE_PRIVATE).getBoolean(EMAIL_NOTIFY_NAME, false));
			}
			holder.switchView.setOnCheckedChangeListener(new OnCheckedChangeListener(){

				@Override
				public void onCheckedChanged(CompoundButton arg0, boolean arg1) {
									
				}			
			});
			}
			else
			{
				if(convertView == null ){
					convertView = inflater.inflate(R.layout.rbm_workout_item, null);
				
					holder = new ViewHolder();
					holder.image = (ImageView) convertView.findViewById(R.id.rbm_item_icon);
					holder.text = (TextView) convertView.findViewById(R.id.rbm_item_text);
				
					convertView.setTag(holder);
					
				}
				else
				{
					holder = (ViewHolder) convertView.getTag();
				}
				
				holder.image.setImageResource(menuItems.get(position).icon);
				holder.text.setText(menuItems.get(position).text);
				
			}
			return convertView;
		}
		
		
		class ViewHolder {
			TextView text;
			ImageView image;
			Switch switchView;
			
		}
			
		
		
		
	}
	


}
