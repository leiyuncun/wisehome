package com.toplion.wisehome;

import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.preference.PreferenceManager;
import android.app.Activity;
import android.app.ActivityGroup;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager.NameNotFoundException;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.RadioGroup.OnCheckedChangeListener;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.net.Socket;
import java.util.Calendar;
import java.util.TimeZone;

import org.json.JSONException;
import org.json.JSONObject;


import com.darvds.ribbonmenu.RibbonMenuView;
import com.darvds.ribbonmenu.iRibbonMenuCallback;
import com.toplion.wisehome.network.ConnectManagerImpl;
import com.toplion.wisehome.network.ConnectServerRequest;
import com.toplion.wisehome.network.DisConnectServerRequest;
import com.toplion.wisehome.network.Request;
import com.toplion.wisehome.network.SendCommandRequest;
import com.toplion.wisehome.network.ServerResponseListener;
import com.toplion.wisehome.util.AccountSaveToLocal;

import android.util.Log;


public class MainActivity extends ActivityGroup {

	Socket socket = null;
	BufferedWriter bw = null;
	BufferedReader br = null;
	
	private long  backkeyeventtime =0;
	// body部分
		private LinearLayout layoutBody;
	// 标题
	private TextView titlebarTitle;
	private RadioGroup btn;
	private RadioButton btnA;
	private RadioButton btnB;
	private RadioButton btnC;
	private RadioButton btnD;
	private RadioButton btnE;
	private RibbonMenuView menuView;
	private ConnectServerRequest mConnectServerRequest = new ConnectServerRequest(new ServerResponseListener<JSONObject>()
	{
		@Override
		public void onServerResponse(JSONObject validResponse) {
				try {						    
						Toast.makeText(MainActivity.this, validResponse.getString("status"), Toast.LENGTH_LONG).show();
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		}
		@Override
		public void onServerError(Throwable error) {
		
			Toast.makeText(MainActivity.this, ""+error, Toast.LENGTH_LONG).show();			
		}		
	});
	
	
	private DisConnectServerRequest mDisConnectServerRequest = new DisConnectServerRequest(new ServerResponseListener<JSONObject>()
	{
		@Override
		public void onServerResponse(JSONObject validResponse) {
			try {		
					Log.e("",validResponse.getString("status"));
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
		@Override
		public void onServerError(Throwable error) {
			
			Toast.makeText(MainActivity.this, ""+error, Toast.LENGTH_LONG).show();
			
		}		
	});
		
	private void initMenuValues() {
	
		menuView = (RibbonMenuView) findViewById(R.id.workoutMenuView);
		menuView.setWithSwitchButton(false);
		menuView.setMenuClickCallback(new iRibbonMenuCallback(){

			@Override
			public void RibbonMenuItemClick(int arg0) {
				if(arg0 == R.id.item1)
				{	
				 LayoutInflater layoutInflater = LayoutInflater.from(MainActivity.this); 
			     View input = layoutInflater.inflate(R.layout.dialog_input, null); 	
			     final EditText ipaddr = (EditText) input.findViewById(R.id.ipaddr);
			     final EditText iport = (EditText) input.findViewById(R.id.iport);
			     
			     ipaddr.setText(PreferenceManager.getDefaultSharedPreferences(MainActivity.this).getString("IP_ADDR", ""));	
			     iport.setText(PreferenceManager.getDefaultSharedPreferences(MainActivity.this).getString("IP_PORT", ""));
			     
				 new AlertDialog.Builder(MainActivity.this,AlertDialog.THEME_HOLO_LIGHT)
				 .setTitle("请输入网络参数")
				 .setIcon(android.R.drawable.ic_input_get)
				 .setView(input)
				 .setPositiveButton("确定",new DialogInterface.OnClickListener() {					
					@Override
					public void onClick(DialogInterface dialog, int which) {
							PreferenceManager.getDefaultSharedPreferences(MainActivity.this)	.edit().putString("IP_ADDR", ipaddr.getText().toString()).commit();
							PreferenceManager.getDefaultSharedPreferences(MainActivity.this)	.edit().putString("IP_PORT", iport.getText().toString()).commit();
					}
				})
				 .setNegativeButton("取消",null)
				 .show();				
				}
				if(arg0 == R.id.item2)
				{
					 LayoutInflater layoutInflater = LayoutInflater.from(MainActivity.this); 
				     View input = layoutInflater.inflate(R.layout.dialog_input_user, null); 	
				     final EditText username = (EditText) input.findViewById(R.id.username);
				     final EditText password = (EditText) input.findViewById(R.id.password);
				     
				     username.setText(PreferenceManager.getDefaultSharedPreferences(MainActivity.this).getString("USERNAME", ""));	
				     password.setText(PreferenceManager.getDefaultSharedPreferences(MainActivity.this).getString("PASSWORD", ""));
				     
					 new AlertDialog.Builder(MainActivity.this,AlertDialog.THEME_HOLO_LIGHT)
					 .setTitle("请输入视频用户名及密码")
					 .setIcon(android.R.drawable.ic_input_get)
					 .setView(input)
					 .setPositiveButton("确定",new DialogInterface.OnClickListener() {					
						@Override
						public void onClick(DialogInterface dialog, int which) {
								PreferenceManager.getDefaultSharedPreferences(MainActivity.this)	.edit().putString("USERNAME", username.getText().toString()).commit();
								PreferenceManager.getDefaultSharedPreferences(MainActivity.this)	.edit().putString("PASSWORD", password.getText().toString()).commit();
						}
					})
					 .setNegativeButton("取消",null)
					 .show();		
				}
				if(arg0 == R.id.item3)
				{	
					
					try {
						 String version = MainActivity.this.getPackageManager().getPackageInfo(MainActivity.this.getPackageName(), 0).versionName;
						 new AlertDialog.Builder(MainActivity.this,AlertDialog.THEME_HOLO_LIGHT)
						 .setTitle("版本信息")
						 .setMessage(version)
						 .setIcon(android.R.drawable.ic_dialog_info)
						 .setPositiveButton("确定",new DialogInterface.OnClickListener() {					
							@Override
							public void onClick(DialogInterface dialog, int which) {
								
							}
						})
					 .show();
					
					} catch (NameNotFoundException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}					 
	
				}
			}
	    	   
	       });
		menuView.setMenuItems(R.menu.main_menu);
	}
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_main_withmenu);
		initMenuValues();
		findViews();
		Intent intent = getIntent();
		final boolean demo = intent.getBooleanExtra("demo", false);
		final String host = intent.getStringExtra("host");
		final String  port = intent.getStringExtra("port");
		
		ConnectManagerImpl.getInstance().ConnectServer(mConnectServerRequest);
					
		ConnectManagerImpl.getInstance().DownloadXML(new Request()
				{
					@Override
					public String getService() {
						// TODO Auto-generated method stub
						String accountArray[] = AccountSaveToLocal.getAccount(MainActivity.this);
						String urlString = "http://"+accountArray[0];
						return urlString;
					}

					@Override
					public JSONObject getParams() throws Exception {
						// TODO Auto-generated method stub
						if(demo)
						{
							return new JSONObject().put("url","http://"+host+":"+port+ConnectManagerImpl.fileName).put("localfile", ConnectManagerImpl.strlocalFile) ;
						}
						String accountArray[] = AccountSaveToLocal.getAccount(MainActivity.this);
						String urlString = "http://"+accountArray[0];
						String portString = accountArray[1];
						return new JSONObject().put("url",urlString+":"+portString+ConnectManagerImpl.fileName).put("localfile", ConnectManagerImpl.strlocalFile) ;
					}

					@Override
					public void handleResponse(JSONObject response) {
						// TODO Auto-generated method stub
						try {						
							Toast.makeText(MainActivity.this, response.getString("status"), Toast.LENGTH_LONG).show();
							btnA.setChecked(true);
						} catch (JSONException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}

					@Override
					public void onError(Throwable error) {
						// TODO Auto-generated method stub
						Toast.makeText(MainActivity.this, ""+error, Toast.LENGTH_LONG).show();
					}

					@Override
					public boolean backgroundThread() {
						// TODO Auto-generated method stub
						return false;
					}

					@Override
					public boolean isUdpRequest() {
						// TODO Auto-generated method stub
						return false;
					}
			
				});
			
		Button btn = (Button)findViewById(R.id.titlebarBtnRight);
		btn.setOnClickListener(new OnClickListener() {			
			@Override
			public void onClick(View v) {
				menuView.toggleMenu();
			}
		});
				
	}

	/**
	 * 标题栏监听和底部菜单
	 */
	private OnCheckedChangeListener btnOnCheckedChangeListener = new OnCheckedChangeListener() {
		@Override
		public void onCheckedChanged(RadioGroup radioGroup, int paramInt) {
			layoutBody.removeAllViews();
			// 首页
			if (paramInt == btnA.getId()) {		
				Intent intent = new Intent(MainActivity.this,
						HomeActivity.class);				
				intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);				
				View view = getLocalActivityManager().startActivity("btnA",
						intent).getDecorView();			
				layoutBody.addView(view);				
			} else if (paramInt == btnB.getId()) {
				Intent intent = new Intent(MainActivity.this,
						RoomActivity.class);				
				intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);				
				View view = getLocalActivityManager().startActivity("btnB",
						intent).getDecorView();			
				layoutBody.addView(view);		
				
			} else if (paramInt == btnC.getId()) {
				Intent intent = new Intent(MainActivity.this,
						QueryActivity.class);				
				intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);				
				View view = getLocalActivityManager().startActivity("btnC",
						intent).getDecorView();			
				layoutBody.addView(view);		
			
			} else if (paramInt == btnD.getId()) {
			//	Intent intent = new Intent(MainActivity.this,
			//			ModeActivity.class);			
				Intent intent = new Intent(MainActivity.this,
						Home_SubActivity.class);		
				intent.putExtra("type","情景模式");				
				intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);				
				View view = getLocalActivityManager().startActivity("btnD",
						intent).getDecorView();			
				layoutBody.addView(view);		
		
			} else if (paramInt == btnE.getId()) {
//				Intent intent = new Intent(MainActivity.this,
				//			SecurityActivity.class);	
				Intent intent = new Intent(MainActivity.this,
						Home_SubActivity.class);		
				intent.putExtra("type","安防控制");
				intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);				
				View view = getLocalActivityManager().startActivity("btnE",
						intent).getDecorView();			
				layoutBody.addView(view);		
				
			}
			
			
		}
	};
	/**
	 * 初始化控件
	 */
	private void findViews() {
		layoutBody = (LinearLayout) findViewById(R.id.layoutBody);
		btn = (RadioGroup) findViewById(R.id.btnGroup);
		btnA = (RadioButton) findViewById(R.id.btnA);
		btnB = (RadioButton) findViewById(R.id.btnB);
		btnC = (RadioButton) findViewById(R.id.btnC);
		btnD = (RadioButton) findViewById(R.id.btnD);
		btnE = (RadioButton) findViewById(R.id.btnE);
		
		btn.setOnCheckedChangeListener(btnOnCheckedChangeListener);
	}
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		ConnectManagerImpl.getInstance().DisconnectServer(mDisConnectServerRequest);	
	}
	
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		// 判断按下的键是返回键.
		if (keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0) {

			if(backkeyeventtime ==0 ||(backkeyeventtime >0 && (event.getEventTime()-backkeyeventtime)>3000)){
				backkeyeventtime = event.getEventTime();
				Toast.makeText(this,"再按一次退出程序",Toast.LENGTH_SHORT).show();
				return true;			
			}else {
				MainActivity.this.finish();
				System.exit(0);
			}	
		}
		return super.onKeyDown(keyCode, event);

	}
}
