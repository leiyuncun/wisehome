package com.toplion.wisehome.util;

import java.util.Dictionary;

import org.json.JSONObject;

import com.toplion.wisehome.network.ConnectManagerImpl;

import android.R.array;
import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;


public class AccountSaveToLocal {

	public static boolean saveAccount(final Context context, final String username, final String password) {
		SharedPreferences sp = context.getSharedPreferences(ConnectManagerImpl.ACCOUNT, context.MODE_PRIVATE);//其中"config"文件名，MODE_PRIVATE为文件的权限；
		Editor editor = sp.edit();//获得编辑这个文件的编辑器；  
		editor.putString(ConnectManagerImpl.ACCOUNTUSERNAMESAVELOCALKEY, username);//利用编辑器编辑内容；
		editor.putString(ConnectManagerImpl.ACCOUNTPASSWORDSAVELOCALKEY, password);//利用编辑器编辑内容；
		boolean isSaveAccount = editor.commit();//调用这个方法提交保存数据。
		return isSaveAccount;
	}
	
	public static String[] getAccount(final Context context) {
		 SharedPreferences sp = context.getSharedPreferences(ConnectManagerImpl.ACCOUNT, context.MODE_PRIVATE);//其中"config"文件名，MODE_PRIVATE为文件的权限；
		 String str_username =sp.getString(ConnectManagerImpl.ACCOUNTUSERNAMESAVELOCALKEY, "");
		 String str_password = sp.getString(ConnectManagerImpl.ACCOUNTPASSWORDSAVELOCALKEY, "");

		 String accountArray[] = new String[2];
		 accountArray[0]=str_username;
		 accountArray[1]=str_password;
		return accountArray;
	}
}
