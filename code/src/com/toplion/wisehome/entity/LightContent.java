package com.toplion.wisehome.entity;

public class LightContent {
	private boolean status;
	private String name;
	private String code;
	private String querycode;
	private int lightLevel = 0;
	private long adLasttime =0;
	
	public LightContent( String name,boolean status,String code,String querycode)
	{
		this.status = status;
		this.name=name;
		this.code = code;
		this.querycode = querycode;
	}
	public String getName(){return name;}
	public boolean getStatus() {return status;}
	public String getCode() {return code;}
	public String getQueryCode() {return querycode;}
	public void setStatus(boolean status) {this.status=status;}
	public void setlightLevel(int lightLevel){this.lightLevel = lightLevel ;}
	public int   getlightLevel(){return this.lightLevel;}
	public void setadLasttime(long time){this.adLasttime = time ;}
	public long   getadLasttime(){return this.adLasttime;}
}
