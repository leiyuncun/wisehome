package com.toplion.wisehome.entity;

public class ModeContent {
	private int id;
	private int icon;
	private String name;
	private boolean status;
	
	
		public final static int outside =1;
		public final static int moive=2;
		public final static int meeting=3;
		public final static int sleeping=4;
		public final static int reading =5;
		public final static int relexing =6;
		public final static int closeall =7;
	
	public ModeContent(int outside,int icon,String name,boolean status)
	{
		this.id =outside;
		this.icon = icon;
		this.name=name;
		this.status = status;
	}
	public String getName(){return name;}
	public int getIcon() {return icon;}
	public int getId(){return id;}
	public boolean getStatus() {return status;}
	
}
