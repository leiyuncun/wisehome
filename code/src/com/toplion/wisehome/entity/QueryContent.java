package com.toplion.wisehome.entity;

public class QueryContent {
	private int icon;
	private String name;
	public QueryContent(int icon,String name)
	{
		this.icon = icon;
		this.name=name;
	}
	public String getName(){return name;}
	public int getIcon() {return icon;}
	
}
