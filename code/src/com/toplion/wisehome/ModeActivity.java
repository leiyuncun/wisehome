package com.toplion.wisehome;

import java.util.ArrayList;
import java.util.List;

import com.toplion.wisehome.adapter.ModeAdapter;
import com.toplion.wisehome.adapter.QueryAdapter;
import com.toplion.wisehome.entity.ModeContent;
import com.toplion.wisehome.entity.QueryContent;



import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.widget.ListView;
import android.widget.TextView;

public class ModeActivity extends Activity {

	ModeAdapter adapter;
	private ListView list;
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mode);        
        list = (ListView) findViewById(R.id.lvlist);
        adapter=new ModeAdapter(this, getData());
        list.setAdapter(adapter);       
        
    }

    private List<ModeContent> getData(){
    	List <ModeContent>dlist=new ArrayList<ModeContent>();    	    	
		dlist.add(new ModeContent(ModeContent.outside,R.drawable.dengguang,getString(R.string.mode_outside),false));
		dlist.add(new ModeContent(ModeContent.sleeping,R.drawable.dengguang,getString(R.string.mode_sleeping),false));	
		return  dlist;
	}

}
