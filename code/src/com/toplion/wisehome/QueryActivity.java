package com.toplion.wisehome;

import java.util.ArrayList;
import java.util.List;

import com.toplion.wisehome.adapter.QueryAdapter;
import com.toplion.wisehome.entity.QueryContent;



import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class QueryActivity extends Activity {

	QueryAdapter adapter;
	private ListView list;
	private List<QueryContent> dlist;
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_query);        
        list = (ListView) findViewById(R.id.lvlist);
        adapter=new QueryAdapter(this, getData());
        list.setAdapter(adapter);       
        list.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				 Toast.makeText(QueryActivity.this, "暂不支持设备查询功能", Toast.LENGTH_LONG).show();			
			}
		}) ;
    }

    private List<QueryContent> getData(){
    	dlist=new ArrayList<QueryContent>();		
		dlist.add(new QueryContent(R.drawable.dengguang,getString(R.string.query_namelight)));
		dlist.add(new QueryContent(R.drawable.dengguang,getString(R.string.query_namecurtain)));
		dlist.add(new QueryContent(R.drawable.dengguang,getString(R.string.query_namemusic)));
		dlist.add(new QueryContent(R.drawable.dengguang,getString(R.string.query_namehomeappliance)));
		dlist.add(new QueryContent(R.drawable.dengguang,getString(R.string.query_nameair)));
		
		return  dlist;
	}

}
