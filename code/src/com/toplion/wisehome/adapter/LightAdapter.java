package com.toplion.wisehome.adapter;

import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import org.apache.commons.codec.binary.Hex;
import org.json.JSONException;
import org.json.JSONObject;



import com.toplion.wisehome.MainActivity;
import com.toplion.wisehome.R;
import com.toplion.wisehome.entity.LightContent;
import com.toplion.wisehome.entity.QueryContent;
import com.toplion.wisehome.network.ConnectManagerImpl;
import com.toplion.wisehome.network.Request;
import com.toplion.wisehome.network.SendCommandRequest;
import com.toplion.wisehome.network.ServerResponseListener;
import com.toplion.wisehome.network.BackgroundServerResponseListener;
import com.toplion.wisehome.util.Tools;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.BaseAdapter;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

public class LightAdapter extends BaseAdapter implements OnClickListener {

	private Context context;
	private List<LightContent> list;

	public LightAdapter(Context context, List<LightContent> list) {
		super();
		this.context = context;
		this.list = list;
	}
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return list.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return list.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	/*
	 * 设置油开关按钮状态的Setview
	 * */
	public void setList_swicth_itemView(View layoutview, final int position){
		    TextView tv = (TextView) layoutview.findViewById(R.id.txtName);
			tv.setText(list.get(position).getName());	
			
			Switch switchView = (Switch)	layoutview.findViewById(R.id.onoffSwitch);
			switchView.setOnCheckedChangeListener(null);
			switchView.setChecked(list.get(position).getStatus());
			
			ImageView titleicon = (ImageView) layoutview.findViewById(R.id.typeIconImage);
			int resourceId = Tools.getImageNameID(context, list.get(position).getName());
			if(resourceId == 0){
				titleicon.setImageResource(R.drawable.tongyong);
			}else{
				titleicon.setImageResource(resourceId);
			}

			switchView.setOnCheckedChangeListener(new OnCheckedChangeListener(){

				@Override
				public void onCheckedChanged(final CompoundButton buttonView,
						final boolean isChecked) {				
						//send request code
						String code = list.get(position).getCode();
						final String[] codearray = code.split(",");
						final String name = list.get(position).getName();
				        String needle1 = "模式";
				        String needle2 = "布防";
				        String needle3 = "撤防";
				        int checkString = name.indexOf(needle1);
				        int checkString2 = name.indexOf(needle2);
				        int checkString3 = name.indexOf(needle3);
				        if(checkString != -1 || checkString2 != -1 || checkString3 != -1){
				        	//TODO 延时3秒执行设置开关状态，只有在场景模式的情况下才执行
				        	new Handler().postDelayed(new Runnable(){   
					            public void run() {
					            	buttonView.setChecked(false);
					            }   

					         }, codearray.length * 300);
				        	
				        	if(isChecked){
				        		 	int index =0;
				        		 	
									for(final String cc: codearray){
										
										byte[] bytes = cc.getBytes();
										//TODO 在场景模式、安防系列的的时候不改变发送出去的控制指令,从xml得到什么就是什么
								        if(checkString == -1 && checkString2 == -1 && checkString3 == -1){
								        	if(isChecked){ 
												bytes[13]=(byte)0x31;
											}else{  
												bytes[13]=(byte)0x32;
											}
								        }
								        
										final String sendString = new String(bytes);
	
										String newcode = new Tools().Parameterstring(sendString);	
										if(newcode!=null){
										    //"3C060001FC010301BC"
											SendCommandRequest openLight = new SendCommandRequest(newcode,new ServerResponseListener<JSONObject>()
												{
													@Override
													public void onServerResponse(JSONObject validResponse) {
														try {	
															Log.i("onServerResponse",validResponse.getString("status"));
															if(cc.equals(codearray[0]))
															Toast.makeText(context, isChecked? name+"成功开启":name+"成功关闭", Toast.LENGTH_SHORT).show();
														} catch (JSONException e) {
															// TODO Auto-generated catch block
															e.printStackTrace();
														}			
													}
													@Override
													public void onServerError(Throwable error) {
														if(cc.equals(codearray[0]))
														 Toast.makeText(context, isChecked? name+"开启失败:"+error : name+"关闭失败:"+error, Toast.LENGTH_LONG).show();
													}		
												});
										
										  ConnectManagerImpl.getInstance().SendCommandDelay(openLight,(index++)*150);								  
									}
							   }
				        		
				        	}
				        }else{
				        	
				        	    int index =0;
						        
								for(final String cc: codearray){
									byte[] bytes = cc.getBytes();
									//TODO 在场景模式的时候不改变发送出去的控制指令，从xml得到什么就是什么
							        if(checkString == -1){
							        	if(isChecked){ 
											bytes[13]=(byte)0x31;
										}else{  
											bytes[13]=(byte)0x32;
										}
							        }
							        
									final String sendString = new String(bytes);

								String newcode = new Tools().Parameterstring(sendString);	
								if(newcode!=null){
								    //"3C060001FC010301BC"
									SendCommandRequest openLight = new SendCommandRequest(newcode,new ServerResponseListener<JSONObject>(){
											@Override
											public void onServerResponse(JSONObject validResponse) {
												try {	
													Log.i("onServerResponse",validResponse.getString("status"));
													if(cc.equals(codearray[0]))
													Toast.makeText(context, isChecked? name+"成功开启":name+"成功关闭", Toast.LENGTH_SHORT).show();
												} catch (JSONException e) {
													// TODO Auto-generated catch block
													e.printStackTrace();
												}			
											}
											@Override
											public void onServerError(Throwable error) {
												if(cc.equals(codearray[0]))
												 Toast.makeText(context, isChecked? name+"开启失败:"+error : name+"关闭失败:"+error, Toast.LENGTH_LONG).show();
											}		
										});
								
								  ConnectManagerImpl.getInstance().SendCommandDelay(openLight,(index)*50);								  
								}
						}
				    }
								
				}			
			});
			
			SeekBar seekbar = (SeekBar) layoutview.findViewById(R.id.lightSeekBar);
			//81~218 : 0~137
			seekbar.setMax(137);		
			//seekbar.setS
			
			final String codeList = list.get(position).getCode();
			final String[] codearray = codeList.split(",");
			final String code = codearray[0];
			byte[] bytes = code.getBytes();
			//3C0600021F0303DC
			byte type = new Tools().uniteBytes(bytes[2], bytes[3]);
			byte roadloop = new Tools().uniteBytes(bytes[10], bytes[11]);
			//判断设备是否是调光灯，和区分是场景模式还是单个设备
			if(((type == 0x02 && roadloop == 0x01)
				|| (type == 0x03 && roadloop == 0x01)	
				|| (type == 0x17 && roadloop >=1 && roadloop<=4 )
				|| (type >= 0x2A && type<= 0x2F && roadloop == 0x01)
				|| (type == 0x19 && roadloop >=1 && roadloop<=2 ))
				&& codearray.length==1){
				seekbar.setVisibility(View.VISIBLE);			
			}else{
				seekbar.setVisibility(View.GONE);
			}
			seekbar.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
				
				@Override
				public void onStopTrackingTouch(SeekBar seekBar) {
					// TODO Auto-generated method stub
					
				}
				
				@Override
				public void onStartTrackingTouch(SeekBar seekBar) {
					// TODO Auto-generated method stub
					
				}			
				@Override
				public void onProgressChanged(SeekBar seekBar, int progress,
						boolean fromUser) {
					final int level = progress;
					//step length should be larger than 15
					//莫余6筛选能被6整除的数发送指令
					if(level%6 == 0){
						String codestring = code.substring(0, 12);
						String openString = String.format("04%X", level+81);
						String closeString = String.format("05%X", level+81);
						String stopstring = codestring + String.format("01%X", level+81);
						
						String controlstring;
						if(list.get(position).getlightLevel()<level){
							//在大于百分之95的灯光的时候将灯光全开
							if(level>125){
								openString = "04DC";
								stopstring = codestring + "04DC";
							}
							controlstring =  codestring+openString;//拼接发送指令
						}else{
							//在小于灯光百分之十的时候将灯光全关
							if(level<10){
								closeString = "05DC";
								stopstring = codestring + "05DC";
							}
							controlstring = codestring+closeString;
						}

						final String newcode = new Tools().Parameterstring(controlstring);
						final String newcode_On = new Tools().Parameterstring(stopstring);
						
						if(newcode!=null){
						//"3C060001FC010301BC"
							SendCommandRequest adjustLight = new SendCommandRequest(newcode,new BackgroundServerResponseListener<JSONObject>()
								{
									@Override
									public void onServerResponse(JSONObject validResponse) {
										
									}
									@Override
									public void onServerError(Throwable error) {
										Log.i("onServerError",""+ error);
									}		
								});
						
						  ConnectManagerImpl.getInstance().SendCommand(adjustLight);
						  list.get(position).setlightLevel(level);
						  list.get(position).setadLasttime(System.currentTimeMillis());
						}
						
						//reopen it
						  SendCommandRequest openLight = new SendCommandRequest(newcode_On,new BackgroundServerResponseListener<JSONObject>()
									{
										@Override
										public void onServerResponse(JSONObject validResponse) {
											
										}
										@Override
										public void onServerError(Throwable error) {
											Log.i("onServerError",""+ error);
										}		
									});
						  ConnectManagerImpl.getInstance().SendCommandDelay(openLight,150);
					}
					
				}
			});
	}
	
	/*
	 * 设置平开模块和上升模块的函数*/
	public void setCurtain_controlView(View layoutview, final int position){
		
		TextView tv = (TextView) layoutview.findViewById(R.id.titleText);
		tv.setText(list.get(position).getName());
		
		ImageView titleimage = (ImageView) layoutview.findViewById(R.id.typeIconImage);
		int resourceId = Tools.getImageNameID(context, list.get(position).getName());
		if(resourceId == 0){
			titleimage.setImageResource(R.drawable.tongyong);
		}else{
			titleimage.setImageResource(resourceId);
		}
		
		
		ImageView openimage = (ImageView) layoutview.findViewById(R.id.curtainOpen);
		openimage.setTag(position);
		openimage.setOnClickListener(this);
		ImageView closeimage = (ImageView) layoutview.findViewById(R.id.curtainClose);
		closeimage.setTag(position);
		closeimage.setOnClickListener(this);
		ImageView stopimage = (ImageView) layoutview.findViewById(R.id.curtainStop);
		stopimage.setTag(position);
		stopimage.setOnClickListener(this);
		
		if(list.get(position).getName().equals("投影屏")||list.get(position).getName().equals("投影幕")){
			if(list.get(position).getStatus()){
				openimage.setSelected(list.get(position).getStatus());
			}else{
				closeimage.setSelected(list.get(position).getStatus());
			}
			
			openimage.setImageResource(R.drawable.curtain_swicth_up_button);
			closeimage.setImageResource(R.drawable.curtain_swicth_down_button);
			stopimage.setImageResource(R.drawable.curtain_swicth_stop_button);
		}else{
			openimage.setImageResource(R.drawable.curtain_swicth_open_button);
			closeimage.setImageResource(R.drawable.curtain_swicth_close_button);
			stopimage.setImageResource(R.drawable.curtain_swicth_stop_button);
		}
		
		if(list.get(position).getStatus()){
			openimage.setSelected(list.get(position).getStatus());
		}else{
			closeimage.setSelected(list.get(position).getStatus());
		}

	}
	
	@Override
	public void onClick(View v) {
		
		int tag = (Integer)v.getTag();
		final String codeList = list.get(tag).getCode();
		final String name = list.get(tag).getName().substring(list.get(tag).getName().length()-1, list.get(tag).getName().length());
		String sendercode = codeList;
		
		switch(v.getId()){
		//cooler
			case R.id.curtainOpen:
				Log.i("test", "窗帘开");
				sendercode = codeList.substring(0, 12)+"01DC";
				break;
				
			case R.id.curtainClose:
				Log.i("test", "窗帘关");
				sendercode = codeList.substring(0, 12)+"02DC";
				break;
				
			case R.id.curtainStop:
				Log.i("test", "窗帘停");
				sendercode = codeList.substring(0, 12)+"00DC";
				break;
		}
		
		String newcode = new Tools().Parameterstring(sendercode);	
		if(newcode!=null)
		{
		//"3C060001FC010301BC"
			SendCommandRequest openLight = new SendCommandRequest(newcode,new ServerResponseListener<JSONObject>()
				{
					@Override
					public void onServerResponse(JSONObject validResponse) {
						try {	
							Log.i("onServerResponse",validResponse.getString("status"));
							
						} catch (JSONException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}			
					}
					@Override
					public void onServerError(Throwable error) {
						
					}		
				});
		
		  ConnectManagerImpl.getInstance().SendCommandDelay(openLight,0);								  
		}
	
	}
	
	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		View layoutview = null;
		LayoutInflater inflater = LayoutInflater.from(context);
		
		if(convertView == null){
			if(list.get(position).getName().equals("窗户") || list.get(position).getName().equals("投影屏")|| list.get(position).getName().equals("投影幕") || list.get(position).getName().substring(0,2).equals("窗帘")){
				layoutview =  inflater.inflate(R.layout.curtain_control_upordown, null);
				setCurtain_controlView(layoutview, position);
			}else{
				layoutview =  inflater.inflate(R.layout.list_swicth_item, null);
				setList_swicth_itemView(layoutview, position);
			}
			
		}else{
			layoutview = convertView;
			if(layoutview.getId() == inflater.inflate(R.layout.list_swicth_item, null).getId()){
				setList_swicth_itemView(layoutview, position);
			}
		}
	
		return layoutview;
	}

}
