package com.toplion.wisehome.adapter;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.toplion.wisehome.HomeActivity;
import com.toplion.wisehome.Home_SubActivity;
import com.toplion.wisehome.R;
import com.toplion.wisehome.Room_SubActivity;
import com.toplion.wisehome.entity.LightContent;
import com.toplion.wisehome.global.Global;
import com.toplion.wisehome.model.Floor;
import com.toplion.wisehome.model.Room;
import com.toplion.wisehome.model.Sensor;
import com.toplion.wisehome.util.PinYin;
import com.toplion.wisehome.util.Tools;


import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class RoomFeatureAdapter extends BaseAdapter {
	private static final String CATEGORY = "no.nordicsemi.android.nrftoolbox.LAUNCHER";

	private final Context mContext;

	private final LayoutInflater mInflater;
	private final List<RoomFeature> mRooms =new ArrayList<RoomFeature>();
    private String type = "�ƹ�";
    

	
	public RoomFeatureAdapter(final Context context,String type) {
		mContext = context;
		if( type!=null)	this.type = type;
		mInflater = LayoutInflater.from(context);

		//�ƹ�
		Floor floor = null ;
    	for(Floor fr : Global.getInstance().building.getmFloors())
       {
    	   if(fr.getName().equals(type))
    	   {
    		   floor  = fr;
    		   break;
    	   }
       }
    	if(mRooms.size()==0){
    		for(Room room:floor.getmRooms()){	
        		if(Tools.getImageNameID(mContext, room.getName())==0){
        			mRooms.add(new RoomFeature(R.drawable.tongyong,room.getName()/*.substring(1+room.getName().indexOf(","))*/));
        		}else{
        			mRooms.add(new RoomFeature(Tools.getImageNameID(mContext, room.getName()),room.getName()/*.substring(1+room.getName().indexOf(","))*/));
        		}

    		}
    	}
    	
    }


	@Override
	public int getCount() {
		return mRooms.size();
	}

	@Override
	public Object getItem(int position) {
		return mRooms.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		View view = convertView;
		if (view == null) {
			view = mInflater.inflate(R.layout.room_feature_icon, parent, false);

			final ViewHolder holder = new ViewHolder();
			holder.view = view;
			holder.icon = (ImageView) view.findViewById(R.id.icon);
			holder.label = (TextView) view.findViewById(R.id.label);
			view.setTag(holder);
		}

	
		final ViewHolder holder = (ViewHolder) view.getTag();
		holder.icon.setImageDrawable(mContext.getResources().getDrawable(mRooms.get(position).icon));
		holder.label.setTextColor(mContext.getResources().getColor( android.R.color.holo_green_dark));
		holder.label.setText(mRooms.get(position).name);
		holder.view.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(mContext, Room_SubActivity.class);
				intent.putExtra("type",type);
				intent.putExtra("name",mRooms.get(position).name);
				mContext.startActivity(intent);		
			}
		});

		return view;
	}

	private class ViewHolder {
		private View view;
		private ImageView icon;
		private TextView label;
	}
	private class RoomFeature {
	    private int icon;
	    private String name;
	    
	    RoomFeature(int icon,String name)
	    {
	    	this.icon = icon;
	    	this.name = name;
	    }
	}
}
