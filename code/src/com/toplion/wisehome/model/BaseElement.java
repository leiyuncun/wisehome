package com.toplion.wisehome.model;

import org.json.JSONException;
import org.json.JSONObject;

public class BaseElement {

	private final static String ID = "Id";
	private final static String NAME = "Name";
	
	private String Id = "0000";
	private String Name ="UNKNOWN";
	public String getId() {
		return Id;
	}
	public void setId(String id) {
		Id = id;
	}
	public String getName() {
		return Name;
	}
	public void setName(String name) {
		Name = name;
	}
	
	public JSONObject ToJson(){
		
		JSONObject json = new JSONObject();
		try {
			json.put(ID, Id);
			json.put(NAME, Name);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
		return json;
	}
	
}
