package com.toplion.wisehome.model;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class Floor  extends BaseElement{
	private List<Room> mRooms = new ArrayList<Room>();

	public List<Room> getmRooms() {
		return mRooms;
	}

	public void setmRooms(List<Room> mRooms) {
		this.mRooms = mRooms;
	}
	
	public JSONObject ToJson()
	{
		JSONObject json = super.ToJson();
		try {
			json.put("Rooms", new JSONArray(mRooms));
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return json;
	}
	
}
