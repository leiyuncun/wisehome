package com.toplion.wisehome;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;

import org.json.JSONException;
import org.json.JSONObject;

import com.toplion.wisehome.network.ConnectManagerImpl;
import com.toplion.wisehome.util.AccountSaveToLocal;

import net.louislam.android.L;


import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

public class LoginActivity extends Activity {
	
	private String mStrUserName;
	private String mStrPwd;
	
	private EditText mUserNameView;
	private EditText mPwdView;
	

	private ProgressDialog mBusyingBar;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		
		requestWindowFeature(Window.FEATURE_NO_TITLE);		
		setContentView(R.layout.activity_login);
		
		mUserNameView = (EditText)findViewById(R.id.etEmailaddress);		
		mPwdView = (EditText)findViewById(R.id.etPassword);
		
		String accountArray[] = AccountSaveToLocal.getAccount(LoginActivity.this);
		mUserNameView.setText(accountArray[0]);
		mPwdView.setText(accountArray[1]);
		
		Button btnLogin = (Button) findViewById(R.id.buttonLogin);		
		btnLogin.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				if(chkValidation())
				{
					Intent intent = new Intent(LoginActivity.this, MainActivity.class);
				    startActivity(intent);
				    LoginActivity.this.finish();
				}
				
			}
		});
		
	

		TextView tvSimplyvisit = (TextView) findViewById(R.id.simplyvisit);		
		tvSimplyvisit.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub					
				Intent intent = new Intent(LoginActivity.this, MainActivity.class);
			    intent.putExtra("host", "hbusdemo.oicp.net");
			    intent.putExtra("port","8300" );
			    intent.putExtra("demo", true);
				startActivity(intent);
			    LoginActivity.this.finish();
			}
		});	
		
		
	
	}
	

	private boolean chkValidation()
	{
		mStrUserName = mUserNameView.getEditableText().toString();
		mStrUserName = mStrUserName.trim();
		mStrPwd = mPwdView.getEditableText().toString();
		
		String accountArray[] = AccountSaveToLocal.getAccount(this);
		mUserNameView.setText(accountArray[0]);
		mPwdView.setText(accountArray[1]);
		
		if (mStrUserName.equals(accountArray[0]) && mStrPwd.equals(accountArray[1]))
		{
			ConnectManagerImpl.strServerName = accountArray[0];
			return true;
		}
		else
		{
			if(mStrUserName.length() >5 && mStrPwd.length()>2){
				
				return AccountSaveToLocal.saveAccount(this,mStrUserName, mStrPwd);
			}else{
				L.alert(this,"用户名或密码不符合要求" );
				
			}
			L.alert(this,"用户名、密码错误，请使用测试用户test/1234或简单访问" );
		}
		return false;
	}

}
