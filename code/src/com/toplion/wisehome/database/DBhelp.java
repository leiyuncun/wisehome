package com.toplion.wisehome.database;

import android.content.Context;
import android.database.sqlite.SQLiteOpenHelper;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

public class DBhelp extends SQLiteOpenHelper{
	private static final String DBNAME = "home.db";
	private static final int VERSION = 1;
	private SQLiteDatabase mDb;
	
	public DBhelp(Context context) {
		super(context, DBNAME, null, VERSION);
	}
	
	@Override
	public void onCreate(SQLiteDatabase db) {		
		mDb = db;
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {		
		//Drop old table here
		onCreate(db);
	}
	
	public void Insert(String fields){

	}
	
	public void Update(String strWhere){
		
	}
	public void Delete(String strWhere){
		
	}
	
	public Cursor Query(String strWhere){	
		return null;
	}
}

