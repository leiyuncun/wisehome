
package com.toplion.wisehome.network;

import org.json.JSONException;
import org.json.JSONObject;


public interface Request {
	
	/*
	 * Called outside of the cloud package
	 */

	/**
	 * Get the service associated with this request, e.g. :
	 * If this is the full url of the cloud service http://imazecorp.com/iMaze/public/service/????????
	 * then ??? can be checkLogin, or createProfile
	 * Then It will return "checkLogin" or "createProfile"
	 * @return the cloud service name
	 */
	public String getService();
	
	/**
	 * Get the parameters of this request.
	 * It is in the form of a flat JSONObject that can be directly sent to the cloud
	 * @return a JSONObject that could be a valid parameter for this request
	 * @throws JSONException 
	 */
	public JSONObject getParams() throws Exception;
	
	/*
	 * END - Called outside of the cloud package
	 */
	
	
	/*
	 * Called by the Connection Manager (or associated classes)
	 */
	
	/**
	 * This function is called by the ConnectionManager when a response is received, this Request should then handle it properly and call convert it into a valid model object or an error
	 * @param the raw JSON response
	 */
	void handleResponse(JSONObject response);
	
	/**
	 * In case of  low level connection erros, the Connection Manager can call this function
	 * @param error
	 */
	void onError(Throwable error);
	
	/**
	 * This function tells us if this request's callbacks should be called in a background thread.
	 * @return true if they should
	 */
	boolean backgroundThread();
	
	/*
	 * END - Called by the Connection Manager (or associated classes)
	 */
	/*
	 * added for UDP request: true is UDP, false is TCP
	 */
	boolean isUdpRequest();
}
