package com.toplion.wisehome;

import java.util.ArrayList;
import java.util.List;

import com.toplion.wisehome.adapter.RoomFeatureAdapter;



import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.widget.GridView;
import android.widget.ListView;
import android.widget.TextView;

public class RoomActivity extends Activity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_room);
        
    	final GridView grid = (GridView) findViewById(R.id.grid);
		grid.setAdapter(new RoomFeatureAdapter(this,null));

    }


}
