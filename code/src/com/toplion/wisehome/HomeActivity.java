package com.toplion.wisehome;

import java.util.ArrayList;
import java.util.List;

import com.toplion.wisehome.adapter.FloorFeatureAdapter;
import com.toplion.wisehome.adapter.RoomFeatureAdapter;



import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.GridView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class HomeActivity extends Activity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        
        final GridView grid = (GridView) findViewById(R.id.grid);
		grid.setAdapter(new FloorFeatureAdapter(this));
        
    }


}
