package com.toplion.wisehome;

import java.util.ArrayList;
import java.util.List;

import com.toplion.wisehome.adapter.ModeAdapter;
import com.toplion.wisehome.adapter.SecurityAdapter;
import com.toplion.wisehome.entity.ModeContent;
import com.toplion.wisehome.entity.SecurityContent;



import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.widget.ListView;
import android.widget.TextView;

public class SecurityActivity extends Activity {

	SecurityAdapter adapter;
	private ListView list;
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_security);
        list = (ListView) findViewById(R.id.lvlist);
        adapter=new SecurityAdapter(this, getData());
        list.setAdapter(adapter);   
    }
    
    private List<SecurityContent> getData(){
    	List <SecurityContent>dlist=new ArrayList<SecurityContent>();   
    	    	
		dlist.add(new SecurityContent(SecurityContent.inside,R.drawable.dengguang,getString(R.string.security_inside),false));
		dlist.add(new SecurityContent(SecurityContent.outside,R.drawable.dengguang,getString(R.string.security_outside),false));	
		return  dlist;
	}


}
