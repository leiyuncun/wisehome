package szy.sdklib;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;

public class RepeatingButton extends Button{
	private OnTouchOnoffListener mOnoffListener;

	public RepeatingButton(Context context)
	{
		super(context);
		SetTouchEvent();
	}
	
	public RepeatingButton(Context context,AttributeSet attrs)
	{
		super(context,attrs);
		SetTouchEvent();
	}
	public RepeatingButton(Context context, AttributeSet attrs, int defStyle)
	{
		super(context,attrs,defStyle);
		SetTouchEvent();
	}
	
	public void SetOnTouchOnoffListener(OnTouchOnoffListener listener)
	{
		mOnoffListener = listener;
	}
	
	public void SetTouchEvent()
	{
		setLongClickable(true);
		setOnTouchListener(new OnTouchListener(){
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				// TODO Auto-generated method stub
				if(event.getAction() == MotionEvent.ACTION_UP)
				{
					if (mOnoffListener!=null) {
						mOnoffListener.OnTouchOnoff(v, MotionEvent.ACTION_UP);
					}
					return false;
				}
				if(event.getAction() == MotionEvent.ACTION_DOWN)
				{
					if (mOnoffListener!=null) {
						mOnoffListener.OnTouchOnoff(v, MotionEvent.ACTION_DOWN);
					}
					return false;
				}
				return false;
			}
		});
	}
	
	public interface OnTouchOnoffListener {
		void OnTouchOnoff(View view,int nMotionEvent);
	}
}
