package szy.sdklib;

import android.app.Activity;
import android.content.Intent;


public class SzySDKlib {

	/**
	 * FunName: startActivity 
	 * Description :  库工程入口
	 * @param ：activity：
	 * @param ：sArrayIP：服务器地址列表
	 * @param ：nPort：服务器端口
	 * @param ：sUser：用户名
	 * @param ：sPassword：密码
	 * @return：void
	 */
	public static void startActivity(Activity activity,String[] sArrayIP,int nPort,String sUser,String sPassword) {
		Intent intent = new Intent();
		intent.putExtra("IPARRAY", sArrayIP);
		intent.putExtra("PORT", nPort);
		intent.putExtra("USER", sUser);
		intent.putExtra("PASSWORD", sPassword);
		intent.setClass(activity, ListActivity.class);
		activity.startActivity(intent);
	}
}
