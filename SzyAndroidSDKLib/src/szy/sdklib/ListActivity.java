package szy.sdklib;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

import szy.utility.NodeInfo;
import szy.utility.SdkHandle2;
import szy.utility.SzyUtility;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.telephony.TelephonyManager;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

public class ListActivity extends Activity {
//	private final int ADD_FIRSTLEVEL_NODE = 1;
//	private final int ADD_CHILD_NODE = 2;
	
	private final int ADD_FIRSTLEVEL_NODE = 11;
	private final int ADD_CHILD_NODE = 12;
	
	private LinkedList<NodeInfo> mListShowingNodes = new LinkedList<NodeInfo>();
	private ListView mListView;
	private TreeViewAdapter mTreeViewAdapter;
	private LinearLayout mLayoutTree;
	private ProgressBar mProgressBar;
	private TextView mTextView;
	
	private HashMap<String, Integer> mMapFrush = new HashMap<String, Integer>();
	private LinkedList<NodeInfo> mListJiedianInfos = new LinkedList<NodeInfo>();
	private Map<String, LinkedList<NodeInfo>> mMapNode = new HashMap<String, LinkedList<NodeInfo>>();

	private String mStrUserid = "";
	private String mStrClubName = "";
	private String mStrServerIP = "";
	@SuppressWarnings("unused")
	private int mIntServerPort = 0;
	private String mStrUsername = "";
	private String mStrPassword = "";
	
	private SzyUtility mSzyUtility;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.tree_view);
		mLayoutTree = (LinearLayout) findViewById(R.id.tree_layout_rect);
		mProgressBar = (ProgressBar) findViewById(R.id.pbar_tree);
		mTextView = (TextView) findViewById(R.id.text_tips);
		mListView = (ListView) findViewById(R.id.list_node);
		
		mTreeViewAdapter = new TreeViewAdapter(this, R.layout.tree_node,
				mListShowingNodes);
		mListView.setAdapter(mTreeViewAdapter);
		mListView.setOnItemClickListener(mOnItemClickListener);
		showProgress();
		mSzyUtility = new SzyUtility();
		String [] sArrayIP = getIntent().getStringArrayExtra("IPARRAY");
		int nPort = getIntent().getIntExtra("PORT", 8006);
		mStrUsername = getIntent().getStringExtra("USER");
		mStrPassword = getIntent().getStringExtra("PASSWORD");

		mSzyUtility.init(sArrayIP, nPort, mSdkHandle2);
		mSzyUtility.login(mStrUsername, mStrPassword, getDeviceKey());
		
		Button btnBack = (Button) findViewById(R.id.btn_back);
		btnBack.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				ListActivity.this.finish();
			}
		});
	}
	
	@Override
	protected void onDestroy() {
		mSzyUtility.release();
		super.onDestroy();
	}
	
	private OnItemClickListener mOnItemClickListener = new OnItemClickListener() {
		@Override
		public void onItemClick(AdapterView<?> arg0, View arg1, int position,
				long arg3) {
			//没有子节点，处在树控件的最低级，即设备节点
			if (!mListShowingNodes.get(position).isbHasChild()) {
				if(mListShowingNodes.get(position).isbAtTerm()){
					Toast.makeText(ListActivity.this, R.string.like_fuwudaoqi, Toast.LENGTH_LONG).show();
				} else if (!mListShowingNodes.get(position).isbAlive()) {
					Toast.makeText(ListActivity.this, R.string.sxtlixian, Toast.LENGTH_LONG).show();
				}else {
					gotoPlayActivity(mListShowingNodes.get(position));
				}
			}
			else {	
				if (mListShowingNodes.get(position).isbExpanded()) {
					//收起树节点
					unExpandedTreeNode(position);
				} 
				else {
					NodeInfo tNode = mListShowingNodes.get(position);
					if (isNeedGetDeviceNode(mListShowingNodes.get(position))) {
						mSzyUtility.getNodeList("1", tNode.getsNodeId(), tNode.getnSxtCount(), tNode.getnJieDianCount());
					}else {
						if (mMapFrush.get(tNode.getsNodeId())==null
								&& (tNode.getnJieDianCount()!=0 || tNode.getnSxtCount()!=0)) {
							mSzyUtility.getNodeList("1", tNode.getsNodeId(), tNode.getnSxtCount(), tNode.getnJieDianCount());
						}
						//展开节点
						expandedTreeNode(position);
					}
				}
			}
		}
		
	};
	
	private void gotoPlayActivity(NodeInfo nodeInfo) {
		Intent intent = new Intent();
		intent.setClass(ListActivity.this, PlayActivity.class);
		intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		intent.putExtra("ID", nodeInfo.getsNodeId());
		intent.putExtra("NAME", nodeInfo.getsNodeName());
		intent.putExtra("PTZ", nodeInfo.isbCloudDev());
		intent.putExtra("IP", mStrServerIP);
		intent.putExtra("PORT", 9023);
		intent.putExtra("USERNAME", mStrUsername);
		intent.putExtra("PASSWORD", mStrPassword);
		startActivity(intent);
	}
	
	// 判断是否需要发送获取设备节点的信息
	public Boolean isNeedGetDeviceNode(NodeInfo nodeInfo) {
		if (nodeInfo.getnJieDianCount() == 0
				&& nodeInfo.getnSxtCount() == 0) {
			return false;
		}
		if (mMapNode.get(nodeInfo.getsNodeId()) != null) {
			return false;
		}
		return true;
	}
	
	private Handler mHandler = new Handler(){
		public void handleMessage(android.os.Message msg) {
			switch (msg.what) {
			case ADD_FIRSTLEVEL_NODE:
				hideProgress();
				mListJiedianInfos.clear();
				@SuppressWarnings("unchecked")
				LinkedList<NodeInfo> lNodeInfos = (LinkedList<NodeInfo>) msg.obj;
				if (lNodeInfos!=null) {
					for (NodeInfo nodeInfo : lNodeInfos) {
						nodeInfo.setnLevel(1);
						nodeInfo.setbHasChild(true);
						nodeInfo.setbHasParent(true);
						nodeInfo.setbCloudDev(false);
						mListJiedianInfos.add(nodeInfo);
					}
				}
				initialData();
				if (mTreeViewAdapter != null) {
					mTreeViewAdapter.notifyDataSetChanged();
				}
				break;
			case ADD_CHILD_NODE:
				addTreeNode(msg);
				break;
			case SzyUtility.LOGIN_SUCCESS:// 登陆成功
				String strMsg = (String) msg.obj;
				if (strMsg.length()>0) {
					showTipsDlg(strMsg, false);
				}
				break;
			case SzyUtility.LOGIN_RET_MSG_ERROR:// 登录数据异常
				showErrorDlg(R.string.login_liebiaohqsb);
				break;
			case SzyUtility.LOGIN_PASSWORD_ERROR:// 用户密码出错
				showErrorDlg(R.string.login_mimabudui);
				break;
			case SzyUtility.LOGIN_USERNAME_ERROR:// 用户名不存在
				showErrorDlg(R.string.login_yonghumingbd);
				break;
			case SzyUtility.LOGIN_CHILD_USER_OVERDATE:// 子账号已过期
				showErrorDlg(R.string.login_zhizhanghaogq);
				break;
			case SzyUtility.LOGIN_ONLINE_MAX_NUM:// 在线人数已达上限
				showErrorDlg(R.string.login_denglushangxianrs);
				break;
			case SzyUtility.LOGIN_OEM_OVERDATE:// OEM已到期
				showErrorDlg(R.string.login_zhanghaoguoqi);
				break;
			case SzyUtility.LOGIN_SERVER_ERROR:// 服务器维护
				String strMsg2 = (String) msg.obj;
				if (strMsg2.length()>0) {
					showTipsDlg(strMsg2, false);
				}
				break;
			case SzyUtility.LOGIN_CONNECT_TIMEOUT:// 连接服务器超时
				showErrorDlg(R.string.login_lianjiefuwuqisb);
				break;
			case SzyUtility.LOGIN_LIST_TIMEOUT:// 获取列表超时
				showErrorDlg(R.string.login_liebiaohuoqusb);
				break;
			default:
				break;
			}
		}
	};
	
	private SdkHandle2 mSdkHandle2 = new SdkHandle2() {
		
		@SuppressWarnings("rawtypes")
		@Override
		public void nodelistCallback(String sId, int nType,
				LinkedList lNodeInfos) {
			Message msg = new Message();
			msg.what = ADD_CHILD_NODE;
			msg.arg1 = nType;
			msg.obj = lNodeInfos;
			mHandler.sendMessage(msg);
		}
		
		@SuppressWarnings("rawtypes")
		@Override
		public void loginCallback(int nRet, String sMsg,
				LinkedList lNodeInfos) {
			switch (nRet) {
			case SzyUtility.LOGIN_SUCCESS:// 登陆成功
				Message msg = new Message();
				msg.what = ADD_FIRSTLEVEL_NODE;
				msg.obj = lNodeInfos;
				mHandler.sendMessage(msg);
				if (sMsg.length()>0) {
					Message msg2 = new Message();
					msg2.what = SzyUtility.LOGIN_SUCCESS;
					msg2.obj = sMsg;
					mHandler.sendMessage(msg2);
				}
				break;
			case SzyUtility.LOGIN_RET_MSG_ERROR:// 登录数据异常
			case SzyUtility.LOGIN_PASSWORD_ERROR:// 用户密码出错
			case SzyUtility.LOGIN_USERNAME_ERROR:// 用户名不存在
			case SzyUtility.LOGIN_CHILD_USER_OVERDATE:// 子账号已过期
			case SzyUtility.LOGIN_ONLINE_MAX_NUM:// 在线人数已达上限
			case SzyUtility.LOGIN_OEM_OVERDATE:// OEM已到期
			case SzyUtility.LOGIN_CONNECT_TIMEOUT:// 连接服务器超时
			case SzyUtility.LOGIN_LIST_TIMEOUT:// 获取列表超时
				mHandler.sendEmptyMessage(nRet);
				break;
			case SzyUtility.LOGIN_SERVER_ERROR:// 服务器维护
				if (sMsg.length() > 0) {
					Message msg2 = new Message();
					msg2.what = SzyUtility.LOGIN_SERVER_ERROR;
					msg2.obj = sMsg;
					mHandler.sendMessage(msg2);
				}
				break;
			default:

				break;
			}
		}

		@Override
		public void serverInfoCallback(String strUserid, String strClubname,String strServerIP, int nServerPort) {
			mStrUserid = strUserid;
			mStrClubName = strClubname;
			mStrServerIP = strServerIP;
			mIntServerPort = nServerPort;
		}
	};
	
	private void initialData() {
    	NodeInfo treeNode = new NodeInfo(mStrUserid,mStrClubName, false, true, "0", 0,true,true);
		mListShowingNodes.add(treeNode);
		
		LinkedList<NodeInfo> linkedListTempNodes = new LinkedList<NodeInfo>();
    	for (int i=0; i<mListJiedianInfos.size();i++) {
    		NodeInfo treeNode1 = mListJiedianInfos.get(i);
			if (treeNode1.getnLevel()==1) {
				linkedListTempNodes.add(treeNode1);
				mListShowingNodes.add(treeNode1);
				if (treeNode1.isbExpanded() && treeNode1.isbHasChild()) {
					AddNode2TreeList(mListShowingNodes.size()-1,treeNode1.getsNodeId(),1);
				}
			}
		}
    	mMapNode.put(treeNode.getsNodeId(), linkedListTempNodes);
	}
	
	private void addTreeNode(Message msg) {
		if (msg.arg1 == 0) {
			@SuppressWarnings("unchecked")
			LinkedList<NodeInfo> lstNodes = (LinkedList<NodeInfo>) msg.obj;
			if (lstNodes.size() > 0) {
				int nPos = getNodeidPos(lstNodes.get(0).getsParentId());
				if (nPos >= 0) {
					AddJiedianNode(mListShowingNodes.get(nPos), lstNodes);
					expandedTreeNode(nPos);
				}
			}
		} else if (msg.arg1 == 1) {
			@SuppressWarnings("unchecked")
			LinkedList<NodeInfo> lstSxts = (LinkedList<NodeInfo>) msg.obj;
			if (lstSxts.size() > 0) {
				int nPos = getNodeidPos(lstSxts.get(0).getsParentId());
				if (nPos >= 0) {
					AddSxtNode(mListShowingNodes.get(nPos), lstSxts);
					expandedTreeNode(nPos);
				}
			}
		}
	}
	 
	private void AddJiedianNode(NodeInfo treeNode, LinkedList<NodeInfo> lstNodes) {
		LinkedList<NodeInfo> linkedListTempNodes = new LinkedList<NodeInfo>();
		for (int i = 0; i < lstNodes.size(); i++) {
			NodeInfo nodeInfo = lstNodes.get(i);
			nodeInfo.setbHasChild(true);
			nodeInfo.setbHasParent(true);
			nodeInfo.setbCloudDev(false);
			nodeInfo.setnLevel(treeNode.getnLevel() + 1);
			linkedListTempNodes.add(nodeInfo);

		}
		mMapNode.put(treeNode.getsNodeId(), linkedListTempNodes);
		mMapFrush.put(treeNode.getsNodeId(), 1);
	}

	private void AddSxtNode(NodeInfo treeNode, LinkedList<NodeInfo> lstSxts) {
		LinkedList<NodeInfo> listTempNodes = new LinkedList<NodeInfo>();
		for (int intPos = 0; intPos < lstSxts.size(); intPos++) {
			NodeInfo sxtInfo = lstSxts.get(intPos);
			sxtInfo.setbExpanded(false);
			sxtInfo.setbHasChild(false);
			sxtInfo.setbHasParent(true);
			sxtInfo.setnLevel(treeNode.getnLevel() + 1);
			sxtInfo.setnJieDianCount(0);
			sxtInfo.setsParentId(treeNode.getsNodeId());
			listTempNodes.add(sxtInfo);
		}

		mMapNode.put(treeNode.getsNodeId(), listTempNodes);
		mMapFrush.put(treeNode.getsNodeId(), 1);
	}
	 
	private int getNodeidPos(String nodeId) {
		int nPos = -1;
		for (int i = 0; i < mListShowingNodes.size(); i++) {
			if (nodeId.equals(mListShowingNodes.get(i).getsNodeId())) {
				nPos = i;
				break;
			}
		}
		return nPos;
	}
	
	//收起树节点
	private void unExpandedTreeNode(int nPos){
		
		mListShowingNodes.get(nPos).setbExpanded(false);
		NodeInfo nodeInfo = mListShowingNodes.get(nPos);
		ArrayList<NodeInfo> temp = new ArrayList<NodeInfo>();
		
		for (int i = nPos+1; i < mListShowingNodes.size(); i++) {
			if (nodeInfo.getnLevel()>=mListShowingNodes.get(i).getnLevel()) {
				break;
			}
			temp.add(mListShowingNodes.get(i));
		}
		mListShowingNodes.removeAll(temp);
		
		mTreeViewAdapter.notifyDataSetChanged();//通知界面更新
	}
	
	// 展开树节点
	private void expandedTreeNode(int nPos) {
		if (mListShowingNodes.get(nPos).isbExpanded()) {
			unExpandedTreeNode(nPos);
		}
		mListShowingNodes.get(nPos).setbExpanded(true);
		AddNode2TreeList(nPos, mListShowingNodes.get(nPos).getsNodeId(),
				1);
		if (mTreeViewAdapter != null) {
			mTreeViewAdapter.notifyDataSetChanged();
		}
	}
	
	private int AddNode2TreeList(int nPos, String nodeId, int j) {
		LinkedList<NodeInfo> listTempNodes =  mMapNode.get(nodeId);
		if (listTempNodes!=null) {
			for (int intPos=0; intPos< listTempNodes.size(); intPos++) {
				NodeInfo treeNode = listTempNodes.get(intPos);
				mListShowingNodes.add(nPos+j, treeNode);
				j++;
				//如果此节点还有子节点而且处于展开状态
				if (treeNode.isbExpanded() && treeNode.isbHasChild()) {
					j = AddNode2TreeList(nPos, treeNode.getsNodeId(), j);
				}
			}
		}
		return j;
	}

	private void showProgress() {
		try {
			mLayoutTree.setGravity(Gravity.CENTER);
			mProgressBar.setVisibility(View.VISIBLE);
			mTextView.setVisibility(View.VISIBLE);
			mListView.setVisibility(View.GONE);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void hideProgress() {
		try {
			mLayoutTree.setGravity(Gravity.TOP);
			mProgressBar.setVisibility(View.GONE);
			mTextView.setVisibility(View.GONE);
			mListView.setVisibility(View.VISIBLE);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private String getDeviceKey(){
 		TelephonyManager tm = (TelephonyManager) this.getSystemService(TELEPHONY_SERVICE);     
		String strKey = tm.getDeviceId();
		if (strKey==null) {
			strKey = String.valueOf(System.currentTimeMillis());
		}
 		return strKey;
 	}
	
	private void showErrorDlg(int nMsg) {
		AlertDialog.Builder builder = new Builder(ListActivity.this);
		builder.setMessage(nMsg);
		builder.setTitle(R.string.dlg_tishi);
		builder.setCancelable(false);
		builder.setPositiveButton(R.string.btn_queding,
				new android.content.DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						dialog.dismiss();
						ListActivity.this.finish();
					}
				});
		builder.create().show();
	}
	
	private void showTipsDlg(String sMsg,final boolean bFlag) {
		AlertDialog.Builder builder = new Builder(ListActivity.this);
		builder.setMessage(sMsg);
		builder.setTitle(R.string.dlg_tishi);
		builder.setCancelable(false);
		builder.setPositiveButton(R.string.btn_queding,
				new android.content.DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						dialog.dismiss();
						if (bFlag) {
							ListActivity.this.finish();
						}
					}
				});
		builder.create().show();
	}
	
}
